//
//  ViewController.swift
//  HitList
//
//  Created by Doru Cojocaru on 04/07/2017.
//  Copyright © 2017 Doru Cojocaru. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var profiles: [NSManagedObject] = []
    var bankAccounts: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Clients List"
        tableView.register(UITableViewCell.self , forCellReuseIdentifier: "Cell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
        //            else { return }
        //
        let managedContext = Profile.shared.persistentContainer.viewContext//appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Profile")
        do {
            profiles = try managedContext.fetch(fetchRequest)
        } catch let error {
            print("couldnt fetch. error: \(error)")
        }
        
        let fetchRequest2 = NSFetchRequest<NSManagedObject>(entityName: "BankAccount")
        do {
            bankAccounts = try managedContext.fetch(fetchRequest2)
        } catch let error {
            print("couldnt fetch. error: \(error)")
        }
    }
    
    @IBAction func unwindToClientList(segue: UIStoryboardSegue) {
        
        let profileListVC = segue.source as? NewProfileVC
        profiles.append((profileListVC!.profile))
        tableView.reloadData()
        
    }
    
    func saveProfile(id: Int, clientId: Int, clientProfile: Int, name: String, type: Int) {
        //        guard let appDelegate = //UIApplication.shared.delegate as? AppDelegate
        //            else {
        //                return
        //        }
        //1
        let managedContext = Profile.shared.persistentContainer.viewContext//appDelegate.persistentContainer.viewContext
        
        //2
        let entity = NSEntityDescription.entity(forEntityName: "Profile", in: managedContext)!
        let profile = NSManagedObject(entity: entity, insertInto: managedContext)
        
        //3
        profile.setValue(id, forKeyPath: "id")
        profile.setValue(clientId, forKeyPath: "clientId")
        profile.setValue(clientProfile, forKeyPath: "clientProfile")
        profile.setValue(name, forKeyPath: "name")
        profile.setValue(type, forKeyPath: "type")
        //4
        do {
            try managedContext.save()
            profiles.append(profile)
        } catch let error as NSError {
            print("Couldnt save because of \(error)")
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientCell", for: indexPath) as? ClientCell
        let profile = profiles[indexPath.row]
        cell?.label.text = profile.value(forKeyPath: "name") as? String
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowClient" {
            let selectedCell = sender as! ClientCell
            let clientVC = segue.destination as? NewProfileVC
            let indexPath = tableView.indexPath(for: selectedCell)
            
            clientVC?.clientProfile = profiles[(indexPath!.row)]
            clientVC?.bankAccounts = bankAccounts
        }
    }
}
