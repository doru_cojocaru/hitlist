//
//  Profile.swift
//  HitList
//
//  Created by Doru Cojocaru on 05/07/2017.
//  Copyright © 2017 Doru Cojocaru. All rights reserved.
//

import Foundation
import CoreData


class Profile {
    
    static let shared = Profile()
    
    
    // MARK: - Core Data stack
    
    var firstName : String? {
        get {
            return UserDefaults.standard.object(forKey: "First Name") as? String
        } set {
            UserDefaults.standard.set(newValue, forKey: "First Name")
        }
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "HitList")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    private init() {}
    
}
