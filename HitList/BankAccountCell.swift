//
//  BankAccountCell.swift
//  HitList
//
//  Created by Doru Cojocaru on 05/07/2017.
//  Copyright © 2017 Doru Cojocaru. All rights reserved.
//

import UIKit

class BankAccountCell: UITableViewCell {


    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
}
