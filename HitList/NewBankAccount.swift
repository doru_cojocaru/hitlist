//
//  NewBankAccount.swift
//  HitList
//
//  Created by Doru Cojocaru on 05/07/2017.
//  Copyright © 2017 Doru Cojocaru. All rights reserved.
//

import UIKit
import CoreData

class NewBankAccount: UIViewController {
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var accountName: UITextField!
    @IBOutlet weak var amount: UITextField!
    
    var bankAccount = NSManagedObject()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let button = sender as? UIButton, button === saveButton {
            let accountName = self.accountName.text
            let amount = Int(self.amount.text!)
            self.saveBankAccount(accountName: accountName!, amount: amount!)
        }
    }
    
    func saveBankAccount(accountName: String, amount: Int) {
        
        let managedContext = Profile.shared.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "BankAccount", in: managedContext)!
        let bankAccount = NSManagedObject(entity: entity, insertInto: managedContext)
        
        bankAccount.setValue(accountName, forKeyPath: "accountName")
        bankAccount.setValue(amount, forKeyPath: "amount")
        
        do {
            try managedContext.save()
            self.bankAccount = bankAccount
        } catch let error {
            print("error of kind: \(error)")
        }
}
}
