//
//  NewProfileVC.swift
//  HitList
//
//  Created by Doru Cojocaru on 04/07/2017.
//  Copyright © 2017 Doru Cojocaru. All rights reserved.
//

import UIKit
import CoreData

class NewProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var idTF: UITextField!
    @IBOutlet weak var clientIDTF: UITextField!
    @IBOutlet weak var clientProfileTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var typeTF: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var id = String()
    var clientID = String()
    var clientProfil = String()
    var name = String()
    var type = String()
    
    var bankAccounts = [NSManagedObject]()
    var profile = NSManagedObject()
    
    var clientProfile : NSManagedObject! {
        didSet {
            id = String (describing: clientProfile.value(forKeyPath: "id")!)
            clientID = String(describing: clientProfile.value(forKeyPath: "clientId")!)
            clientProfil = String(describing: clientProfile.value(forKeyPath: "clientProfile")!)
            name = clientProfile.value(forKeyPath: "name") as! String
            type = String(describing: clientProfile.value(forKeyPath: "type")!)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        idTF.text = id
        clientIDTF.text = clientID
        clientProfileTF.text = clientProfil
        nameTF.text = name
        typeTF.text = type
        
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let button = sender as? UIButton, button === saveButton {
            let id = Int(idTF.text!)
            let clientID = Int(clientIDTF.text!)
            let clientProfile = Int(clientProfileTF.text!)
            let name = nameTF.text
            let type = Int(typeTF.text!)
            self.saveProfile(id: id!, clientId: clientID!, clientProfile: clientProfile!, name: name!, type: type!)
        }
    } 
   
    
    func saveProfile(id: Int, clientId: Int, clientProfile: Int, name: String, type: Int) {
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
//            else { return }
        //1
        let managedContext = Profile.shared.persistentContainer.viewContext
        //appDelegate.persistentContainer.viewContext
        
        //2
        let entity = NSEntityDescription.entity(forEntityName: "Profile", in: managedContext)!
        let profile = NSManagedObject(entity: entity, insertInto: managedContext)
        
        //3
        profile.setValue(id, forKeyPath: "id")
        profile.setValue(clientId, forKeyPath: "clientId")
        profile.setValue(clientProfile, forKeyPath: "clientProfile")
        profile.setValue(name, forKeyPath: "name")
        profile.setValue(type, forKeyPath: "type")
        //4
        do {
            try managedContext.save()
            self.profile = profile
        } catch let error as NSError {
            print("Couldnt save because of \(error)")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bankAccounts.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == (tableView.numberOfRows(inSection: 0) - 1) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddAccountCell", for: indexPath) as? AddAccountCell
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BankAccountCell", for: indexPath) as? BankAccountCell
            let bankAccount = bankAccounts[indexPath.row]
            cell?.accountNameLabel.text = (bankAccount.value(forKeyPath: "accountName") as! String)
            cell?.amountLabel.text = String(describing: bankAccount.value(forKeyPath: "amount")!)
            return cell!
        }
        
    }
    
    @IBAction func unwindToBankAccounts(segue: UIStoryboardSegue) {
        let bankAccountsTV = segue.source as? NewBankAccount
        bankAccounts.append(bankAccountsTV!.bankAccount)
        tableView.reloadData()
    }
    
    
    
}
